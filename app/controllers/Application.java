package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.F.Function;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

/**
 * Main controller serves all HTTP requests.
 */
public class Application extends Controller {

	/**
	 * Main method for accessing the site. Returns the index.html view
	 * @return
	 */
    public static Result index() {
    	System.out.println("index ->");
        return ok(index.render("Euro Exchange Rates"));
    }

	/**
	 * Gets the currencies as a promise with a JSON string array containing the currencies.
	 * @return A Promise with a JSON string array containing the currencies, for example ["USD", "SEK"]
	 * If successful the resulting JSON will be:
	 * {"result":"success", "values"=["USD", "SEK"]}
	 * If failed it will be:
	 * {"result":"error"}
	 */
    public static Promise<Result> getCurrencies() {
		return model.DataStorage.getCurrencies().map(new Function<JsonNode, Result>() {
			public Result apply(JsonNode json) {
				return ok(json);
			}
		});
    }

	/**
	 * Gets the exchange rates for a currency as a promise with a JSON Array of objects.
	 * @return A Promise with a JSON  array containing the exchange rates, for example:
	 * [{"date":"2014-01-20","currency":"USD","rate":"1.4623"}, {"date":"2014-01-21","currency":"USD","rate":"1.4623"}]
	 * If successful the resulting JSON will be:
	 * {"result":"success", "values"=[{"date":"2014-01-20","currency":"USD","rate":"1.4623"}, {"date":"2014-01-21","currency":"USD","rate":"1.4623"}]}
	 * If failed it will be:
	 * {"result":"error"}
	 */
    public static Promise<Result> getExchangeRates(String currency) {
		return model.DataStorage.getExchangeRates(currency).map(new Function<JsonNode, Result>() {
			public Result apply(JsonNode json) {
				return ok(json);
			}
		});
    }

    /**
     * Reloads all the exchange rates from the ECB web service and saves them into the DB.
     * The resulting JSON is an object with result set to either success or error.
     * @return a JSON object with success or failure, e.g {"result":"success"} or {"result":"error"}
     */
    public static Promise<Result> refreshAndGetExchangeAllRates() {
		return model.DataStorage.refreshAndGetExchangeAllRates().map(new Function<JsonNode, Result>() {
			public Result apply(JsonNode json) {
				return ok(json);
			}
		});
    }

//    public static Result read() {
//    	DataStorage.testRead();
//    	return ok("DB read");
//    }
//
//    public static Result insert() {
//    	DataStorage.testInsert();
//    	return ok("DB insert");
//    }
//    
//    public static Result reset() {
//    	DataStorage.reset();
//    	return ok("Reset");
//    }

}
