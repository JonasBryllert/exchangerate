package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.libs.WS;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;

/**
 * This class serves as DAO and tries to get requested data from the
 * Cassandra DB. If not found all the exchange rates for 90 days are 
 * downloaded from the ECB web service.
 *
 */
public abstract class DataStorage {
	
//	private static boolean getAndSaveExchangeRatesIsOngoing = false;
	private static RequestQueue queue = new RequestQueue();
	
	/**
	 * Holder class for an instance of an exchange rate
	 * @author jbryller
	 *
	 */
	private static class ExchangeRate {
		public ExchangeRate() {}
		public ExchangeRate(String date, String currency, String rate) {
			this.date = date;
			this.currency = currency;
			this.rate = rate;
		}
		
		public String date;
		public String currency;
		public String rate;
		public JsonNode toJsonNode() {
			ObjectNode on = JsonNodeFactory.instance.objectNode();	
			on.put("date", date);
			on.put("currency", currency);
			on.put("rate", rate);
			return on;
		}
		public String toString() {
			return toJsonNode().toString();
		}
	}

	
	//******** CASSANDRA set up START **********
	private static boolean initialized = false;
	//Column Family
	private static final ColumnFamily<String, String> EXCHANGE_RATES =
			  new ColumnFamily<String, String>(
			    "exchangerates",              // Column Family Name
			    StringSerializer.get(),   // Key Serializer
			    StringSerializer.get());  // Column Serializer

	private static Keyspace keyspace = null;
	
	public static void initialize() {
		if (initialized) {
			System.out.println("initialize already done");
			return;
		}
    	AstyanaxContext<Keyspace> context = new AstyanaxContext.Builder()
        .forCluster("ClusterName")
        .forKeyspace("demo")
        .withAstyanaxConfiguration(new AstyanaxConfigurationImpl()      
            .setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE)
        )
        .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("MyConnectionPool")
            .setPort(9160)
            .setMaxConnsPerHost(1)
            .setSeeds("127.0.0.1:9160")
        )
        .withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
        .buildKeyspace(ThriftFamilyFactory.getInstance());

		System.out.println("initialize context created: " + context);

    	context.start();
		System.out.println("initialize context started: " + context);
    	keyspace = context.getEntity();
		System.out.println("initialize keyspace: " + keyspace);
		initialized = true;
	}
	//********* CASSANDRA set up END ***********
	
	
	/**
	 * Gets the exchange rates synchronously from web service and stores them in the DB.
	 * @return
	 */
	private static List<ExchangeRate> getAndSaveExchangeRates() {
		System.out.println("getAndSaveExchangeRates ->");
//		getAndSaveExchangeRatesIsOngoing = true;
		queue.setBusy();
		WS.Response response = WS.url("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml").get().get();
  	    Document doc = response.asXml();
  	    List<ExchangeRate> list = xmlToJava(doc);
  	    System.out.println("getAndSaveExchangeRates -> " + list.size());
  	    //Clear any content in the DB and insert new data
  	    clearDB();
  	    insertInDB(list);
//  	    getAndSaveExchangeRatesIsOngoing = false;
  	    queue.setIdle();
  	    return list;
	}
	
	/**
	 * Convert XML from ECB to a list of ExchangeRate
	 * @param doc
	 * @return
	 */
	private static List<ExchangeRate> xmlToJava(Document doc) {
		ArrayList<ExchangeRate> exchangeRateList = new ArrayList<ExchangeRate>();
        NodeList cubes = doc.getElementsByTagName("Cube");
        System.out.println("xmlToJava -> cubes : " + cubes.getLength());
        
        //Unpack the cubes nodes and save them into ExchangeRate
        String date = null;
        for (int i = 0; i < cubes.getLength(); i++) {
        	Node e = cubes.item(i);
        	if (e.hasAttributes()) {
        		NamedNodeMap atts = e.getAttributes();
        		Node timeNode = atts.getNamedItem("time");
        		if (timeNode != null) {
        			date = timeNode.getNodeValue();
        			continue;
        		}
        		//Find currency and rate
        		Node currencyNode = atts.getNamedItem("currency");
        		Node rateNode = atts.getNamedItem("rate");
        		if (currencyNode != null && rateNode != null) {
                	ExchangeRate er = new ExchangeRate();
                	er.date =  date;
                	er.currency = currencyNode.getNodeValue();
                	er.rate = rateNode.getNodeValue();
                	System.out.println("Adding ER: " + er.toString());
                	exchangeRateList.add(er);       			
        		}
        	}
        	
        }
        return exchangeRateList;
	}
	
	/**
	 * Gets the exchange rates from the DB as a JsonNode.
	 * If anything nothing in hte DB of not validating throw exception. 
	 * @param currency
	 * @return a JsonNode containing the array of
	 */
	private static List<String> getCurrenciesFromDB() {
		System.out.println("getCurrenciesFromDB ->");
		initialize();
		ArrayList<String> list = new ArrayList<>();
		try {
			OperationResult<CqlResult<String, String>> result = keyspace.prepareQuery(EXCHANGE_RATES).withCql("SELECT currency FROM exchangerates").asPreparedStatement().execute();
			if (result != null && result.getResult() != null) {
				Rows<String, String> rows = result.getResult().getRows();	
				HashSet<String> set = new HashSet<>();
				for (String key : rows.getKeys()) {
					Row<String, String> row = rows.getRow(key);
					ColumnList<String> columns = row.getColumns();
					// Lookup columns in response by name
					String cur = columns.getColumnByName("currency").getStringValue();
					set.add(cur);
				}
				list.addAll(set);
				Collections.sort(list);
				//Special treat for USD. Add it also first as is the default selection.
				if (list.contains("USD")) list.add(0, "USD");
			}
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		System.out.println("currency result: " + list);

		if (list.isEmpty()) {
			System.out.println("getCurrenciesFromDB -> Nothing in DB, throw exception.");
			throw new RuntimeException("No currencies in DB");
		}
		return list;
	}
	
	/**
	 * Gets the exchange rates from the DB as a JsonNode.
	 * If anything nothing in hte DB of not validating throw exception. 
	 * @param currency
	 * @return a JsonNode containing the array of
	 */
	private static List<ExchangeRate> getExchangeRatesFromDB(String currency) {
		System.out.println("getExchangeRatesFromDB ->");
		initialize();
		ArrayList<ExchangeRate> list = new ArrayList<>();
		try {
			OperationResult<CqlResult<String, String>> result = keyspace.prepareQuery(EXCHANGE_RATES).withCql("SELECT * FROM exchangerates WHERE currency='" + currency + "'").asPreparedStatement().execute();
			if (result != null && result.getResult() != null) {
				Rows<String, String> rows = result.getResult().getRows();	
				for (String key : rows.getKeys()) {
					Row<String, String> row = rows.getRow(key);
					ColumnList<String> columns = row.getColumns();
					// Lookup columns in response by name
					String date = columns.getColumnByName("date").getStringValue();
					String cur = columns.getColumnByName("currency").getStringValue();
					Float rate = columns.getColumnByName("rate").getFloatValue();
					list.add(new ExchangeRate(date, cur, rate.toString()));
				}
				Collections.sort(list, new Comparator<ExchangeRate>() {
					public int compare(ExchangeRate er1, ExchangeRate er2) {
						return er1.date.compareTo(er2.date);
					};
				});
			}
		} catch (ConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); 
		}
		System.out.println("exchange rate list from DB result: " + list);

		return list;
	}
	
	/**
	 * Converts a list of ExcangeRate to a JSon array
	 * @return a JsonNode which will be a ArrayNode
	 */
	private static JsonNode exchangeRateListToJSon(List<ExchangeRate> list) {
		//List in DB, return JSON
		ArrayNode an = JsonNodeFactory.instance.arrayNode();
		for (ExchangeRate er : list) {
			an.add(er.toJsonNode());
		}
		System.out.println("getExchangeRatesFromDB -> result size: " + list.size());
		return an;
	}
	
	/**
	 * Converts a list of strings to a JSon array
	 * @return a JsonNode which will be a ArrayNode
	 */
	private static JsonNode stringListToJSon(List<String> list) {
		ArrayNode an = JsonNodeFactory.instance.arrayNode();
		for (String s : list) {
			an.add(s);
		}
		return an;
	}
	
	/**
	 * Crude verification that it roughly gives correct amount of days.
	 * SHould really check that the last day workday exist and that first day is 3 month back.
	 * So just check that list contains between 40 - 70 days (always at least 20 weekend days in 3 months)
	 * @param list
	 */
	public static void validateExchangeRateList(List<ExchangeRate> list) {
		if (list.size() < 40 || list.size() > 70) throw new RuntimeException("List size wrong " + list.size());
	}
	
	/**
	 * Gets the currencies available. Tries teh DB first, if fails it loads from 
	 * ECB web service.
	 * @return A Promise with a JSON object containing the currencies. 
	 * If successful it will be:
	 * {"result":"success", "values"=["USD", "SEK"]}
	 * If failed it will be:
	 * {"result":"error"}
	 */
	public static Promise<JsonNode> getCurrencies() {
		System.out.println("getCurrencies -> ");
		Promise<JsonNode> promiseOfDB = Promise.promise(new Function0<JsonNode>() {
			public JsonNode apply() {
//				check DB to see if already loaded
				waitIfgetAndSaveExchangeRatesIsOngoing();
				List<String> exchangeRateList = getCurrenciesFromDB();
				System.out.println("getCurrencies <- success");
				return returnSuccessfulJsonResult(stringListToJSon(exchangeRateList));
			}
		}).recover(new Function<Throwable, JsonNode>() {
			public JsonNode apply(Throwable t) {
//				end up here if DB does not contain result
//				check first if another thread/user is already loading hte 
//				exchange rates
				if (!waitIfgetAndSaveExchangeRatesIsOngoing()){
					getAndSaveExchangeRates();
				}
				List<String> exchangeRateList = getCurrenciesFromDB();
				System.out.println("getCurrencies <- success webservice");
				return returnSuccessfulJsonResult(stringListToJSon(exchangeRateList));
			}

		}).recover(new Function<Throwable, JsonNode>() {
			public JsonNode apply(Throwable t) {
//				give up and return dummy for time being
				ObjectNode o = JsonNodeFactory.instance.objectNode();
				o.put("error", "error");
				System.out.println("getCurrencies <- error");
				return o;
			}
		});
		
		return promiseOfDB;
	}
	
	/**
	 * Gets the exchange rates for a currency as a promise with a JSON Array of objects.
	 * @return A Promise with a JSON  array containing the exchange rates, for example:
	 * If successful it will be:
	 * {"result":"success", "values"=[{"date":"2014-01-20","currency":"USD","rate":"1.4623"}, {"date":"2014-01-21","currency":"USD","rate":"1.4623"}]}
	 * If failed it will be:
	 * {"result":"error"}
	 */
	public static Promise<JsonNode> getExchangeRates(final String currency) {
		System.out.println("getExchangeRate -> " + currency);
		Promise<JsonNode> promiseOfDB = Promise.promise(new Function0<JsonNode>() {
			public JsonNode apply() {
//				Wait if loading of exchange rates is ongoing
				waitIfgetAndSaveExchangeRatesIsOngoing();
				List<ExchangeRate> exchangeRateList = getExchangeRatesFromDB(currency);
				validateExchangeRateList(exchangeRateList);
				System.out.println("getExchangeRate <- success" + currency);
				return returnSuccessfulJsonResult(exchangeRateListToJSon(exchangeRateList));
			}
		}).recover(new Function<Throwable, JsonNode>() {
			public JsonNode apply(Throwable t) {
//				end up here if DB does not contain result
//				check first if another thread/user is already loading hte 
//				exchange rates
				if (!waitIfgetAndSaveExchangeRatesIsOngoing()){
					getAndSaveExchangeRates();
				}
				List<ExchangeRate> exchangeRateList = getExchangeRatesFromDB(currency);
				System.out.println("getExchangeRate <- success webservice" + currency);
				return returnSuccessfulJsonResult(exchangeRateListToJSon(exchangeRateList));
			}

		}).recover(new Function<Throwable, JsonNode>() {
			public JsonNode apply(Throwable t) {
//				give up and return dummy for time being
				ObjectNode o = JsonNodeFactory.instance.objectNode();
				o.put("error", "error");
				System.out.println("getExchangeRate <- error" + currency);
				return o;
			}
		});
		
		return promiseOfDB;
	}
	
    /**
     * Reloads all the exchange rates from the ECB web service and saves them into the DB.
     * The resulting JSON is an object with result set to either success or error.
     * @return a JSON object with success or failure, e.g {"result":"success"} or {"result":"error"}
     */
	public static Promise<JsonNode> refreshAndGetExchangeAllRates() {
		System.out.println("refreshAndGetExchangeAllRates -> ");
		Promise<JsonNode> promiseOfDB = Promise.promise(new Function0<JsonNode>() {
			public JsonNode apply() {		
//				check first if another thread/user is already loading the exchange rates
				if (!waitIfgetAndSaveExchangeRatesIsOngoing()){
					getAndSaveExchangeRates();
				}

				return returnSuccessfulJsonResult(null);
			}

		}).recover(new Function<Throwable, JsonNode>() {
			public JsonNode apply(Throwable t) {
//				give up and return dummy for time being
				ObjectNode o = JsonNodeFactory.instance.objectNode();
				o.put("result", "error");
				return o;
			}
		});
		
		return promiseOfDB;
	}
	
	private static JsonNode returnSuccessfulJsonResult(JsonNode value) {
		ObjectNode o = JsonNodeFactory.instance.objectNode();
		o.put("result", "success");
		if (value != null) {
			o.put("value", value);
		}
		return o;
	}
	
	/**
	 * Crude way of avoiding many requests to load exchange rates at the same time
	 */
	private static boolean waitIfgetAndSaveExchangeRatesIsOngoing() {
		return queue.waitUntilIdle();
	}

//	private static boolean waitIfgetAndSaveExchangeRatesIsOngoing() {
//		boolean hasWaited = getAndSaveExchangeRatesIsOngoing;
//		while (getAndSaveExchangeRatesIsOngoing) {
//			try {
//				Thread.sleep(300);
//			}
//			catch (InterruptedException e) {/*Ignore*/}
//		}
//		return hasWaited;
//	}

	
	private static void insertInDB(List<ExchangeRate> list) {
		initialize();
		System.out.println("insert initialized");
    	MutationBatch m = keyspace.prepareMutationBatch();

    	for (ExchangeRate er : list) {
	    	m.withRow(EXCHANGE_RATES, er.date + er.currency)
	    	  .putColumn("date", er.date)
	    	  .putColumn("currency", er.currency)
	    	  .putColumn("rate", new Float(er.rate));
    	}
		try {
			m.execute();
//			OperationResult<Void> result = m.execute();
			System.out.println("insert finished");

		} catch (ConnectionException e) {
			// TODO Should do something more useful
			e.printStackTrace();
		}
	}
	
	private static void clearDB() {
		initialize();
		try {
			keyspace.truncateColumnFamily(EXCHANGE_RATES);
		} catch (ConnectionException e) {
			// TODO Should do something more useful
			e.printStackTrace();
		}
	}
	
//	public static void testInsert() {
//		initialize();
//		System.out.println("insert initialized");
//    	MutationBatch m = keyspace.prepareMutationBatch();
//
//    	m.withRow(EXCHANGE_RATES, "testing")
//    	  .putColumn("date", "2014-01-10")
//    	  .putColumn("currency", "JBY")
//    	  .putColumn("rate", 12.3F);
////  	  .putColumn("date", "2014-01-10", null)
////  	  .putColumn("currency", "JBY", null)
////  	  .putColumn("rate", "99", null);
//		System.out.println("insert finished");
//		try {
//			  OperationResult<Void> result = m.execute();
//		} catch (ConnectionException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	public static void testRead() {
//		initialize();
//		OperationResult<ColumnList<String>> result;
//		try {
//			result = keyspace.prepareQuery(EXCHANGE_RATES).getKey("testing")
//					.execute();
//			System.out.println("read result: " + result);
//			ColumnList<String> columns = result.getResult();
//
//			// Lookup columns in response by name
//			String date = columns.getColumnByName("date").getStringValue();
//			String currency = columns.getColumnByName("currency").getStringValue();
//			Float rate = columns.getColumnByName("rate").getFloatValue();
//			System.out.println("read date: " + date);
//			System.out.println("read currency: " + currency);
//			System.out.println("read rate: " + rate);
//
//			// Or, iterate through the columns
//			for (Column<String> c : result.getResult()) {
//				System.out.println(c.getName());
//			}
//		} catch (ConnectionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	public static List<ExchangeRate> testRead2() {
//		initialize();
//		ArrayList<ExchangeRate> list = new ArrayList<>();
//		try {
//			OperationResult<CqlResult<String, String>> result = keyspace.prepareQuery(EXCHANGE_RATES).withCql("SELECT * FROM exchangerates WHERE currency='USD'").asPreparedStatement().execute();
//			if (result != null && result.getResult() != null) {
//				Rows<String, String> rows = result.getResult().getRows();	
//				for (String key : rows.getKeys()) {
//					Row<String, String> row = rows.getRow(key);
//					ColumnList<String> columns = row.getColumns();
//					// Lookup columns in response by name
//					String date = columns.getColumnByName("date").getStringValue();
//					String currency = columns.getColumnByName("currency").getStringValue();
//					Float rate = columns.getColumnByName("rate").getFloatValue();
//					list.add(new ExchangeRate(date, currency, rate.toString()));
//				}
//			}
//		} catch (ConnectionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace(); 
//		}
//		System.out.println("read2 result: " + list);
//		return list;
//	}
//	
//	public static void reset() { 
//		initialized = false;
//	}
//		
}
