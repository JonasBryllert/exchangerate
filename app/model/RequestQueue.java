package model;

/**
 * Implements a simple queue mechanism where a user
 * can call waitUntilLocked() which will block until
 * the queue is idle.
 *
 */
public class RequestQueue {
	
	private Object lock = new Object();
	private boolean isIdle = true;
	private int queueCount = 0;
	
	/**
	 * Will block while queue is busy and then release.
	 */
	public boolean waitUntilIdle() {
		synchronized (lock) {
			if (isIdle) return false;
			else {
				System.out.println("WAiting for idle queue, count: " + queueCount);
				queueCount++;
				waitForLock();
				return true;
			}
		}
	}
	
	private void waitForLock() {
		try {
			lock.wait();
		} catch (InterruptedException e) {
		}
	}
	
	/**
	 * Set the queue to busy which will lock callers of waitUntilIdle
	 */
	public void setBusy() {
		synchronized (lock) {
			isIdle = false;
		}
	}

	/**
	 * Set the queue to idle which will unlock callers of waitUntilIdle
	 */
	public void setIdle() {
		synchronized (lock) {
			isIdle = true;
			queueCount = 0;
			lock.notifyAll();
		}
	}
}
