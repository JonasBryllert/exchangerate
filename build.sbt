name := "exchangerate"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)

libraryDependencies ++= Seq (
	"com.netflix.astyanax" % "astyanax" % "1.56.26" 
)	

play.Project.playJavaSettings
