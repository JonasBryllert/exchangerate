/**
 * 
 */
var busy = false;

$(document).ready(function(){
	$(".action").attr("disabled", "disabled");
	
	$("#currencySelector").change(function(){
		if (busy) return;
		console.log("currencySelector clicked : " + $(this).val())
		loadExchangeRates($(this).val());
	});

	$("#refreshAll").click(function(){
		if (busy) return;
		console.log("refreshAll clicked ")
		refreshAllExchangeRates();
	});

	$("#refreshCurrent").click(function(){
		if (busy) return;
		console.log("refreshCurrent clicked ")
		loadExchangeRates($("#currencySelector").val());
	});

  loadCurrencies();

});

/**
 * Loads the currencies to be displayed in the select box using a JSON rest call
 */
function loadCurrencies() {
	console.log("loadCurrences ->");
	message("Please wait. Loading currencies...");
	busy = true;
	$.getJSON("currencies", function(data) {
		//The data returned is just an array of strings (currencies)
		console.log("loadCurrences data: " + data.result);
		if (data.result == "success") {
			var items = [];
			$.each(data.value, function(key,currency) {
			    items.push( "<option value='" + currency + "'>" + currency + "</option>" );
			});
			var allOptions = items.join("");
			console.log("loadCurrences allOptions : " + allOptions)
			$("#currencySelector").empty();
			$("#currencySelector").append(allOptions)
			loadExchangeRates(data.value[0])	
		}
		else {
			message("No exchanges found. Please try later.")	
			busy = false;
		}
	});
}

/**
 * Loads the exchange rates for a currency using a JSON rest call.
 * @param currency
 */
function loadExchangeRates(currency) {
	console.log("loadExchangeRates -> " + currency);
	busy = true;
	$(".action").attr("disabled", "disabled");
	$("#summaryDiv").hide();
	$(".chart").hide();
	message("Please wait. Loading exchange rates...");
	$("#loadingDiv").show();
	$("<svg></svg>").addClass("chart").replaceAll(".chart");
	
	$.getJSON("exchangerates/" + currency, function(data) {
		//The data returned is just an array of objects with properties currency, rate, date
		console.log("loadExchangeRates data: " + data.result);
		if (data.result == "success") {
			addSummary(data.value);
			calculateGraph(data.value);
			$("#loadingDiv").hide();
			$("#summaryDiv").show();
			$(".chart").show();
			$(".chartDiv").html($(".chartDiv").html());
			$(".action").removeAttr("disabled"); 
			busy = false;
		}
		else {
			message("No exchange rates found. Please try later.")
		}
	});
}

/**
 * This function reloads all exchange rates from ECB web service.
 * and then calls to refresh the currently selected.
 */
function refreshAllExchangeRates() {
	console.log("refreshAllExchangeRates -> ");
	busy = true;
	$(".action").attr("disabled", "disabled");
	$("#summaryDiv").hide();
	$(".chart").hide();
	message("Please wait. Reloading exchange rates from ECB...");
	$("#loadingDiv").show();
	
	$.getJSON("reloadexchangerates", function(data) {
		//The data returned is objects with one properties result which can be 'success' or 'error'
		console.log("loadExchangeRates data: " + data.result);
		if (data.result == "success") {
			loadExchangeRates($("#currencySelector").val())
		}
		else {
			message("Failed to reload exchanges. Please come back later.");
//			busy = false;
		}
	});
}	

/**
 * Displays a message in the DIV used when loading data etc
 * @param message
 */
function message(message) {
	$("#loadingDiv h1").text(message)
}

/**
 * Adds data to the summary table
 * @param data
 */
function addSummary(data) {
	$("#summaryTable > tbody").empty()
	
	var startDate = data[0].date;
	var startRate = data[0].rate;
	$("#summaryTable > tbody").append(
			"<tr><td>First day</td><td>"+startDate+"</td><td>"+startRate+"</td></tr>");
	
	var endDate = data[data.length - 1].date;
	var endRate = data[data.length - 1].rate;
	$("#summaryTable > tbody").append(
			"<tr><td>End day</td><td>"+endDate+"</td><td>"+endRate+"</td></tr>");
	
	var max = data.reduce(function(prev, next) {
		if (!prev.rate) return next;
		if (prev.rate > next.rate) return prev;
		return next;
	});
	$("#summaryTable > tbody").append(
			"<tr><td>Max</td><td>"+max.date+"</td><td>"+max.rate+"</td></tr>");
	
	var min = data.reduce(function(prev, next) {
		if (!prev.rate) return next;
		if (prev.rate < next.rate) return prev;
		return next;
	});
	$("#summaryTable > tbody").append(
			"<tr><td>Min</td><td>"+min.date+"</td><td>"+min.rate+"</td></tr>");
}

/**
 * Calculates and add the SVG graph.
 * @param data
 */
function calculateGraph(data) {
//	d3.select(".chart").remove();
//	d3.select("body").append("svg");
	var margin = {top: 60, right: 30, bottom: 80, left: 40};
    var width = 960 - margin.left - margin.right;
    var height = 550 - margin.top - margin.bottom;
    
    var barWidth = (width / data.length);
	console.log("barWidth -> " + barWidth);
	barWidth = 5;

	var xMin = getDate(data[0]);
	var xMax = getDate(data[data.length-1]);
	console.log("startdate: " + xMin + ", end date: " + xMax);

	function getDate(d) {
	    return new Date(d.date);
	}

	var x = d3.time.scale().domain([xMin, xMax]).range([0, width]); 
	console.log("x(min):" + x(xMin) + "x(max):" + x(xMax));
		
	var y = d3.scale.linear()
	    .range([height, 0])
		.domain([0, d3.max(data, function(d) { return d.rate; })]);
		
	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom")
    	.ticks(d3.time.mondays)
//    	.ticks(d3.time.mondays, 1)
    	.tickFormat(d3.time.format('%a %d-%m-%y'));
	
	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");
	
	var chart = d3.select(".chart")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
//Dynamic part start	
	  chart.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	      .selectAll("text")  
            .style("text-anchor", "end")
            .attr("dx", "-.8em")
            .attr("dy", ".15em")
            .attr("transform", function(d) {
                return "rotate(-90)" 
                });	  
	
	  chart.append("g")
	      .attr("class", "y axis")
	      .call(yAxis);
	
	  var bar = chart.selectAll("r")
	    .data(data)
	    .enter().append("g")
//	    .attr("transform", function(d, i) { return x(getDate(d)); });
	    .attr("transform", function(d, i) { return "translate(" + x(getDate(d)) + "," + 0 + ")"; });
	  
	  bar.append("rect")
	      .attr("class", "bar")
//	      .attr("x", function(d, i) { return x(getDate(d)); })
	      .attr("y", function(d) { return y(d.rate); })
	      .attr("height", function(d) { return height - y(d.rate); })
	      .attr("width", barWidth/*x.rangeBand()*/);
	  
	  bar.append("text")
	    .attr("y", function(d) {
	    	return 0; 
		})    
	    .attr("dx", "2px")
//	    .attr("dy", "2em")
	    .text(function(d) { return d.rate; })
	    .style("writing-mode", "tb")
	    .style("fill", "black");

	//Dynamic part end	

}

